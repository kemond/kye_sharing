"""
# All Training Utils

---------------------------------------------------------------------------------------------------------------------------------

Kye Emond

June 15, 2023              
                                                                                                   
---------------------------------------------------------------------------------------------------------------------------------

A module designed to help training a neural network for cross-section ratio estimation, using all events to their maximum \
potential
"""

# Imports
import numpy as np
import pandas as pd
import tensorflow as tf
import keras as k
import typing as ty

# Useful functions for preprocessing
def events_to_training(events: pd.DataFrame, 
                       coef_values: ty.Iterable[float],
                       c_value_prefix: str = "weight_cHW_", 
                       SM_weight_title: str = "weight_sm", 
                       kinematic_columns: np.ndarray = np.arange(2, 39), 
                       randomize_data: bool = True) -> tuple[np.ndarray, np.ndarray]:
    """Return the training data as well as an array of coefficients and weights to use for calculating loss

    Args:
        * events (pd.DataFrame): The events used for training
        * coef_values (Iterable[float]): The coefficient values you want to use for fitting
        * c_value_prefix (str, optional): The prefix to the titles of the weight columns. Defaults to "weight_cHj3_".
        * SM_weight_title (str, optional): The title of the standard model weight column. Defaults to "weight_sm".
        * kinematic_columns (np.ndarray, optional): An array of indices that have the kinematic variables. \
            Defaults to np.arange(2, 22).
        * randomize_data (bool, optional): Whether to randomize the output order of the data. Defaults to True.

    Returns:
        tuple[np.ndarray, np.ndarray]: A tuple holding the training data as well as the array used for loss calculation.
    """
    
    # Get a numpy array with all the kinematic variables per event
    return_kinematics = events.to_numpy()[:, kinematic_columns]
    
    # Get an array of the standard model weights for each event
    sm_weights = np.expand_dims(events[SM_weight_title].to_numpy(), 1)
    
    # Get an array of the SMEFT weights for each event
    all_weights_arr = np.array([events[c_value_prefix + float_to_string(value)] for value in coef_values]).T
    
    # Get an array of all the weights
    return_weights = np.concatenate((sm_weights, all_weights_arr), axis=1)
    
    # Randomize the data
    shuffled_indices = np.random.permutation(len(return_kinematics)) if randomize_data else np.arange(len(return_kinematics))
    
    # Return the values calculated
    return return_kinematics[shuffled_indices], return_weights[shuffled_indices]
    

def float_to_string(number: float) -> str:
    string = str(number).replace(".", "p").replace("-", "neg")
    if "neg" not in string:
        string = "pos" + string
    return string


def string_to_float(string: str) -> float:
    number_bit = string.split("_")[-1].replace("pos", "").replace("neg", "-").replace("p", ".")
    return float(number_bit)
    

# Model building
def simple_deep_dense_net(input_shape: tuple, 
                          output_neurons: int = 1, 
                          hidden_layers: int = 2, 
                          neurons_per_layer: int = 32, 
                          activation: str = "relu", 
                          dropout_frac: float = 0.1) -> k.models.Model:
    """Return a deep, densely connected neural network with the same topology per layer

    Args:
        * input_shape (tuple): The shape of the input data
        * output_neurons (int, optional): The number of output neurons. Defaults to 1.
        * hidden_layers (int, optional): The number of hidden layers. Defaults to 1.
        * neurons_per_layer (int, optional): The number of neurons in each hidden layer. Defaults to 32.
        * activation (str, optional): The activation type of the neurons. Defaults to "relu".
        * dropout_frac (float, optional): The fraction of neurons to be dropped every cycle. Defaults to 0.1.

    Returns:
        Model: A simple deep dense neural network
    """
    
    net = k.models.Sequential()
    net.add(k.layers.Dense(neurons_per_layer, activation, input_shape=input_shape))
    if dropout_frac > 0.0:
        net.add(k.layers.Dropout(dropout_frac))
    for _ in range(hidden_layers - 1):
        net.add(k.layers.Dense(neurons_per_layer, activation))
        if dropout_frac > 0.0:
            net.add(k.layers.Dropout(dropout_frac))
    net.add(k.layers.Dense(output_neurons))
    
    return net


def f(n_alpha: tf.Tensor, n_beta: tf.Tensor, coefs: tf.Tensor) -> tf.Tensor:
    return 1.0 / (1.0 + (1.0 + coefs * n_alpha) ** 2.0 + (coefs * n_beta) ** 2.0)


def build_loss(training_coefs: ty.Iterable) -> ty.Callable[[tf.Tensor, tf.Tensor], tf.Tensor]:
    """Build and return a loss function using the given training coefficients

    Args:
        training_coefs (ty.Iterable): An iterable containing the coefficients used for training

    Returns:
        ty.Callable[[tf.Tensor, tf.Tensor], tf.Tensor]: The loss function to use for training
    """
    
    # Get an appropriately reshaped array of coefficients
    coefs_array = tf.reshape(training_coefs, (1, len(training_coefs)))
    
    # Define the function to be returned
    def loss(weights: tf.Tensor, network_output: tf.Tensor) -> tf.Tensor:
        """Return the loss evaluated with the given weights and network outputs

        Args:
            weights (tf.Tensor): The event weights. Axis 0 should be which event you're looking at. \
                Along axis 1, the 0th element should be the SM weight. Beyond that, the weights should \
                be in the same order as the coefs_array. 
            network_output (tf.Tensor): The output of the neural network. Axis 0 should be which event you're looking at. \
                In the 0th position of axis 1, you should have the alpha network output, and in the 1st position, the beta.

        Returns:
            tf.Tensor: The loss evaluated for each event.
        """
        
        return tf.math.reduce_sum(
            (tf.abs(weights[:, 0, tf.newaxis]) + tf.abs(weights[:, 1:])) 
             * (f(tf.squeeze(network_output[:, 0])[:, tf.newaxis], tf.squeeze(network_output[:, 1])[:, tf.newaxis], coefs_array)
                 - 1.0 / (1.0 + tf.abs(weights[:, 1:] / weights[:, 0, tf.newaxis]))) ** 2.0, 
             axis=1)
    
    # Return the created loss function
    return loss

# Unit Tests
if __name__ == "__main__":
    
    # Test f is working
    alpha_test = tf.constant(((1.0,), (2.0,), (3.0,)))
    beta_test = tf.constant(((5.0,), (6.0,), (7.0,)))
    coefs_test = tf.constant(((-1.0, 1.0),))
    f_output = tf.constant(((1.0 / 26.0, 1.0 / 30.0), 
                            (1.0 / 38.0, 1.0 / 46.0), 
                            (1.0 / 54.0, 1.0 / 66.0)))
    
    f_success = tf.reduce_all(tf.math.equal(f(alpha_test, beta_test, coefs_test), f_output))
    
    if f_success:
        print("PASSED: f test")
    else:
        print(f"FAILED: f test: Difference of {f(alpha_test, beta_test, coefs_test) - f_output}")
    
    
    # Test build_loss is working
    coefs_test_2 = (-1.0, 1.0)
    weights = tf.constant(((1.0, 2.0, 0.0), 
                           (2.0, 0.0, 2.0), 
                           (3.0, 1.0, 1.0)))
    network_output = tf.concat((alpha_test, beta_test), axis=1)
    loss_output = tf.reduce_sum(tf.constant(((3.0, 1.0), 
                                             (2.0, 4.0), 
                                             (4.0, 4.0))) * (f_output - tf.constant(((1.0 / 3.0, 1.0), 
                                                                                     (1.0, 1.0 / 2.0), 
                                                                                     (3.0 / 4.0, 3.0 / 4.0)))) ** 2.0, 
                                             axis=1)
    
    loss = build_loss(coefs_test_2)
    
    build_loss_success = tf.reduce_all(tf.math.equal(loss(weights, network_output), loss_output))
    
    if build_loss_success:
        print("PASSED: build_loss test")
    else:
        print(f"FAILED: build_loss test: Difference of {loss(weights, network_output) - loss_output}")