"""
# All Training Utils

---------------------------------------------------------------------------------------------------------------------------------

Kye Emond

June 15, 2023

---------------------------------------------------------------------------------------------------------------------------------

A module designed to help training a neural network for cross-section ratio estimation, using all events to their maximum \
potential
"""

# Imports
import sys
import numpy as np
import uproot as ur
import matplotlib.pyplot as plt
import tensorflow as tf
import keras as k
import typing as ty

from all_training_utils import events_to_training, build_loss, simple_deep_dense_net, string_to_float, float_to_string


# Main function
def main() -> None:
    
    # Loading data
    PATH_TO_DATA = "/Users/kyeemond/Desktop/Work Stuff/Higgs/event_trees_small/nTuples-stat-learning-new.root"
    C_VALUE_PREFIX = "weight_cHj3_"
    KINEMATIC_COLUMNS = np.array((4, 5, 6, 7, 12, 13, 14, 15, 20, 21, 22, 23, 24, 25, 28, 29, 30, 33, 35, 36, 37, 38))#np.arange(2, 39)
    RANDOMIZE_DATA = True
    
    # What data to use for training
    TRAINING_SLICE = slice(None, -10000)
    BATCH_SIZE = None
    COEF_VALUES = (-0.5, -0.2, -0.1, 0.1, 0.2, 0.5)
    
    # Network configuration
    HIDDEN_LAYERS = 4
    DROPOUT_FRAC = 0.0
    
    # Training settings
    EPOCHS = 500_000
    PATIENCE = 1000
    MIN_DELTA = 0.0
    VALIDATION_SPLIT = 0.1
    
    # Checkpointing
    CHECKPOINT_PATH = None
    CHECKPOINT_PERIOD = 20
    
    # Saving
    SAVE_DIRECTORY = "/Users/kyeemond/Desktop/Work Stuff/Higgs/AllModel/Results"
    NETWORK_NAME = "cHj3_model_all_loss"
    
    # Cheating
    USE_WEIGHTS = False
    WEIGHT_COLUMNS = np.array((*range(39, 47), *range(79, 86)))
    
    
    
    # Set up the training on as many GPUs as available
    strategy = tf.distribute.MirroredStrategy()
    print(f"Number of devices: {strategy.num_replicas_in_sync}")
    
    # Start defining variables on all those GPUs
    with strategy.scope():
        # Get the training data
        training_data, weights = get_data(filepath=PATH_TO_DATA, 
                                          training_slice=TRAINING_SLICE, 
                                          coef_values=COEF_VALUES, 
                                          c_value_prefix=C_VALUE_PREFIX, 
                                          kinematic_columns=KINEMATIC_COLUMNS if not USE_WEIGHTS else WEIGHT_COLUMNS, 
                                          randomize_data=RANDOMIZE_DATA, 
                                          use_weights=USE_WEIGHTS)
        
        # Get the saved model if we want to start from a model that's already in training
        saved_model_path = get_saved_model_path()
        
        # Build the model
        model = (build_model(training_data=training_data, 
                             hidden_layers=HIDDEN_LAYERS, 
                             dropout_frac=DROPOUT_FRAC, 
                             training_coefs=COEF_VALUES) 
                 if saved_model_path is None
                 else k.models.load_model(saved_model_path, custom_objects={"loss": build_loss(COEF_VALUES)}))
    
    # Train the model
    history = train_model(model=model, 
                          training_data=training_data, 
                          weights=weights, 
                          batch_size=BATCH_SIZE, 
                          epochs=EPOCHS, 
                          patience=PATIENCE, 
                          min_delta=MIN_DELTA, 
                          checkpoint_path=CHECKPOINT_PATH, 
                          checkpoint_frequency=int(CHECKPOINT_PERIOD 
                                                * (np.ceil(training_data[TRAINING_SLICE].shape[0] / BATCH_SIZE) 
                                                   if BATCH_SIZE is not None else 1.0)), 
                          validation_split=VALIDATION_SPLIT)
    
    # Save everything
    save_run(model=model, history=history, network_name=NETWORK_NAME, save_directory=SAVE_DIRECTORY)


def get_data(filepath: str, 
             training_slice: slice, 
             coef_values: ty.Iterable, 
             c_value_prefix: str, 
             kinematic_columns: np.ndarray, 
             randomize_data: bool, 
             use_weights: bool) -> tuple[np.ndarray, np.ndarray]:
    """Return properly formatted data for training

    Args:
        * filepath (str): Path to the root nTuples
        * training_slice (slice): The slice of events to use for training
        * coef_values (Iterable): The values of the coefficients to train on
        * c_value_prefix (str): The prefix of the coefficient values in the root nTuple columns
        * kinematic_columns (ndarray): A numpy array of indices where you can find the kinematic variables
        * randomize_data (bool): Whether to randomize the output data
        * use_weights (bool): Whether to use the true weights as training variables

    Returns:
        * ndarray: The input variables for training
        * ndarray: The weights for the loss function
    """
    
    # Load the data into a pandas DataFrame
    with ur.open(filepath) as file:
        events = file["HWWTree_emme;1"].arrays(library="pandas")

    # Properly normalize all the data
    for name in events.columns:
        if "weight_" in name:
            events[name] *= events["weight"]
    
    # Return the data
    training_data, weights = events_to_training(events=events[training_slice], 
                                                          coef_values=coef_values, 
                                                          c_value_prefix=c_value_prefix, 
                                                          kinematic_columns=kinematic_columns, 
                                                          randomize_data=randomize_data)
    
    if use_weights:
        training_data = training_data / np.expand_dims(training_data[:, 0], -1)
    
    return training_data, weights


def get_saved_model_path() -> str | None:
    """Return the path to the file passed to --filepath or -f when the script was called

    Returns:
        str | None: The path to the desired file
    """
    
    # Get the set of arguments passed when running the script
    arguments = sys.argv
    # Iterate through and check if there was a filepath
    for arg in arguments:
        if "--filepath" in arg or "-f" in arg:
            return arg.split("=")[1]
    # Return None if not
    return None


def build_model(training_data: tf.Tensor, 
                hidden_layers: int, 
                dropout_frac: float, 
                training_coefs: ty.Iterable) -> k.models.Model:
    """Returns the constructed DNN

    Args:
        * training_data (Tensor): The input variables to train on
        * hidden_layers (int): The number of hidden layers to use
        * dropout_frac (float): The fraction for the dropout layers
        * training_coefs (Iterable): The values of the coefficients to train on

    Returns:
        * Model: The built model
    """
    
    # Define sequential models n_alpha and n_beta, as in the paper
    n_alpha = simple_deep_dense_net(training_data.shape[1:], hidden_layers=hidden_layers, dropout_frac=dropout_frac)
    n_alpha._name = "n_alpha"
    n_beta = simple_deep_dense_net(training_data.shape[1:], hidden_layers=hidden_layers, dropout_frac=dropout_frac)
    n_beta._name = "n_beta"

    # Define the functional model that makes use of n_alpha and n_beta to train them
    inputs = k.Input(shape=training_data.shape[1:])
    norm_layer = k.layers.Normalization()
    norm_layer.adapt(training_data)
    norm_layer = norm_layer(inputs)
    n_alpha_output = n_alpha(norm_layer)
    n_beta_output = n_beta(norm_layer)
    joined_output = k.layers.Concatenate()((n_alpha_output, n_beta_output))
    full_model = k.models.Model(inputs=inputs, outputs=[joined_output])
    
    # Compile the model
    full_model.compile(optimizer="adam", loss=build_loss(training_coefs))
    
    return full_model


def train_model(model: k.models.Model, 
                training_data: tf.Tensor, 
                weights: tf.Tensor, 
                batch_size: int | None, 
                epochs: int, 
                patience: int, 
                min_delta: float, 
                checkpoint_path: str | None, 
                checkpoint_frequency: int, 
                validation_split: float) -> k.callbacks.History:
    """Train the model and return its history

    Args:
        * model (k.models.Model): The model to train
        * training_data (tf.Tensor): The input data on which to train the model
        * weights (tf.Tensor): The weights to use for calculating the loss
        * batch_size (int | None): The size of the batched. If None, it is set to the size of the entire dataset
        * epochs (int): The number of epochs for which to train
        * patience (int): The number of epochs for which loss has to stay constant before the training is stopped early
        * min_delta (float): The minimum change in loss to prevent early stopping
        * checkpoint_path (str | None): The path to the directory where you want to store checkpoints. \
            If None, no checkpoints are stored
        * checkpoint_frequency (int): The number of epochs to wait between saves.
        * validation_split (float): The fraction of the input training data to use for validation

    Returns:
        * k.callbacks.History: The history of the training
    """
    
    # Fit the model
    return model.fit(x=training_data,
                     y=weights,
                     batch_size=batch_size if batch_size is not None else training_data.shape[0],
                     epochs=epochs,
                     callbacks=[k.callbacks.EarlyStopping("val_loss" if validation_split > 0.0 else "loss", 
                                                          patience=patience, 
                                                          min_delta=min_delta, 
                                                          restore_best_weights=True), 
                                *((k.callbacks.ModelCheckpoint(checkpoint_path, 
                                                               monitor="val_loss" if validation_split > 0.0 else "loss", 
                                                               mode="min", 
                                                               save_best_only=True, 
                                                               save_weights_only=False, 
                                                               save_freq=checkpoint_frequency),) 
                                  if checkpoint_path is not None else tuple())], 
                     validation_split=validation_split)


def save_run(model: k.models.Model, 
             history: k.callbacks.History, 
             network_name: str, 
             save_directory: str) -> None:
    
    # Plot the loss over time and save
    plot_metrics(history=history, network_name=network_name, save_directory=save_directory)
    
    # Save the models
    print("MESSAGE TO USER: Do not worry if you get messages saying compiled metrics have yet to be built. "
          "They are not supoosed to be built")
    save_models(model=model, network_name=network_name, save_directory=save_directory)


def plot_metrics(history: k.callbacks.History, network_name: str, save_directory: str) -> None:
    """Plot and save the loss function for a given network

    Args:
        * history (History): The history to use for plotting the loss
        * network_name (str): The name to give the plot
        * save_directory (str): The directory in which to save the plot
    """
    
    # Plot training and validation loss
    plt.figure()
    plt.plot(history.history["loss"], label="Training Loss")
    if "val_loss" in history.history.keys():
        plt.plot(history.history["val_loss"], label="Validation Loss")
    plt.title(network_name)
    plt.xlabel("Epochs")
    plt.ylabel("Loss")
    plt.yscale("log")
    plt.legend()
    plt.savefig(f"{save_directory}/{network_name}_loss.png")


def save_models(model: k.models.Model, network_name: str, save_directory: str) -> None:
    """Save the trained model and its submodels

    Args:
        * model (Model): The model to save
        * network_name (str): The name to give the network
        * save_directory (str): The directory in which to save the models
    """
    
    # Save the model
    model.save(f"{save_directory}/{network_name}")

    # Extract the alpha and beta subnetworks and save them
    n_alpha_trained = extract_submodel(model, ("input_1", "normalization", "n_alpha"))
    n_alpha_trained.save(f"{save_directory}/{network_name}_n_alpha")

    n_beta_trained = extract_submodel(model, ("input_1", "normalization", "n_beta"))
    n_beta_trained.save(f"{save_directory}/{network_name}_n_beta")


def extract_submodel(model: k.models.Model, layers: tuple[str, ...]) -> k.models.Model:
    """Return a sequential model built from the selected layers

    Args:
        * model (Model): The model to extract layers from
        * layers (tuple[str, ...]): The names of the layers to extract

    Returns:
        * Model: A sequential model built from the chosen layers
    """
    
    submodel = k.models.Sequential()
    for layer in layers:
        submodel.add(model.get_layer(layer))
    return submodel


# Call the main function
if __name__ == "__main__":
    main()